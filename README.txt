CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Node Like/Dislike Field module is a fully customizable module which allows
visitors to like, dislike and report abuse about the node anonymously. This
module defines a field type that can be added to any content type. This field
integrates the statistical graphs into the node edit page which allows the
administrator to have knowledge about the total number of likes and dislikes
and users statistics according to selected date in 1 to 15 days.

 * For a full description of the module visit:
   https://www.drupal.org/project/node_like_dislike_field

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/node_like_dislike_field


REQUIREMENTS
------------

All dependencies of this module are enabled by default in Drupal 8.x.


INSTALLATION
------------

Install the Node Like/Dislike Field module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Node Like/Dislike
       Field module.
    2. Navigate to Administration > Structure > Content types > [content type to
       edit] > Manage fields and select "+ Add field".
    3. Select "Likes Dislikes" from the dropdown menu. Enter a label name.
       Save and continue.
    4. Select number of values: Limited and count or Unlimited. Save field
       settings.
    5. Enter any default values if desired. Save settings.
    6. After creating content, the saved node now has buttons for "Like",
       "Dislike", and "Report Abuse".


MAINTAINERS
-----------

 * Chandra Shekhar Paatni (ShekharPaatni) -
   https://www.drupal.org/u/shekharpaatni
 * Aayushi Garg (aayushi2601) - https://www.drupal.org/u/aayushi2601

Project maintained and supported by:

 * TO THE NEW - https://www.drupal.org/to-the-new
